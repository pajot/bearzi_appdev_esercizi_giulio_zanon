function calcolaGiorniPassati(datainiziale)
    {
     if(typeof datainiziale !== "object")
         throw new Error("il valore del parametro non è una data");
     var millisecondiDaData = Date.now()-datainiziale.getTime();
     var secondi = millisecondiDaData/1000;
     var minuti = secondi /60;
     var ore= minuti/60;
     var giorni = ore/24;
     return Math.floor(giorni);
    }

function calcolaGiorniMancanti(datafinale)
{
    if(typeof datafinale !== "object")
        throw new Error("il valore del parametro non è una data");
    var millisecondiADataFinale = datafinale.getTime()-Date.now();
    var secondi = millisecondiADataFinale/1000;
    var minuti = secondi /60;
    var ore= minuti/60;
    var giorni = ore/24;
    return Math.floor(giorni);                                                          //arrotondamento per difetto
}

var cristmas2016 = new Date(2016,11,25);
var cristmas2017 = new Date(2017,11,25);
var cristmas2018 = new Date(2018,11,25);

console.log("i giorni passati da natale 2016 sono "+calcolaGiorniPassati(cristmas2016));
console.log("i giorni mancanti a natale 2017 sono "+calcolaGiorniMancanti(cristmas2017));
console.log("i giorni mancanti a natale 2018 sono "+calcolaGiorniMancanti(cristmas2018));