var Lifeform = function (){};
Lifeform.prototype.isLifeform = true;
Lifeform.prototype.heartbeat = function ()
    {console.log("........");}

var Animal = function () {}
Animal.prototype = Lifeform.prototype;
Animal.prototype.isAnimal = true;

var Mammal = function () {}
Mammal.prototype = Animal.prototype;
Mammal.prototype.isMammal = true;

var Cat = function (species)
    {this.species = species;}
Cat.prototype = Mammal.prototype;
Cat.prototype.isCat = true;


var lf = new Lifeform();
var an =new Animal();
var ma =new Mammal();
var tiger =new Cat("tiger");

console.log("lf.isLifeform: "+lf.isLifeform);
lf.heartbeat();
console.log("an.isLifeform: "+an.isLifeform);
console.log("an.isAnimal: "+an.isAnimal);
an.heartbeat();
console.log("ma.isLifeform: "+ma.isLifeform);
console.log("ma.isAnimal: "+ma.isAnimal);
console.log("ma.isMammal: "+ma.isMammal)
ma.heartbeat();
console.log("tiger.isLifeform: "+tiger.isLifeform);
console.log("tiger.isAnimal: "+tiger.isAnimal);
console.log("tiger.isMammal: "+tiger.isMammal);
console.log("tiger.isCat: "+tiger.isCat);
console.log("tiger.species: " +tiger.species);
tiger.heartbeat();
for (var i in tiger)
{
    console.log("indice: "+i);
    console.log("valore: "+ tiger[i]);
}

