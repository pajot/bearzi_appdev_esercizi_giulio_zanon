/* funzione ottimizzata per implementare l'ereditarietà da mamma microsoft <3*/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var Persona = /** classe madre "persona"*/ (function () {
    function Persona(name, age) {
        this.nome = "";
        this.età = NaN;
    }
    return Persona;
}());

/* atleta */
var Atleta = /** classe figlio */ (function (_super) {
    __extends(Atleta, _super);
    function Atleta(nome, età, velocità) {
        var _this = _super.call(this, nome, età) || this;
        _this.velocità = 40;
        return _this;
    }
    /* sovrascrive il metodo aggiungendone funzionalità */
    Atleta.prototype.corri = function () {
        var out = NaN;
        this.
        return out;
    };
    return Atleta;
}(Persona));
