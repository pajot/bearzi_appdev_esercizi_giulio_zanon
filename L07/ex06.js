var recuperaEstensione = function (nomefile) {
    var out = "";
    var posizione = nomefile.lastIndexOf(".");// identifico la posizione dell'ultiomo "." per poter poi leggere la estensione
    //console.log(posizione);
    out = nomefile.substr(posizione+1);
    return out;
}

console.log(recuperaEstensione("manuale.pdf"));
console.log(recuperaEstensione("foto.jpg"));
console.log(recuperaEstensione("curriculum2017.doc"));
console.log(recuperaEstensione("prova.txt"));