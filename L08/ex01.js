console.log("esercizio 01 lezione 08");

/* funzione ottimizzata per implementare l'ereditarietà */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var Persona = /** @class */ (function () {
    function Persona(nome, eta) {
        this.nome = nome;
        this.eta = eta;
    }
    Persona.prototype.profilazione = function () {
        console.log(this.nome+" è una persona di "+this.eta+" anni")
    };
    return Persona;
}());

/* estende Person */
var Atleta = /** @class */ (function (_super) {
    __extends(Atleta, _super);
    function Atleta(nome, eta, velocita) {
        var _this = _super.call(this, nome, eta) || this;
        _this.velocita = velocita;
        return _this;
    }

    Atleta.prototype.profilazione = function () {
        var out = _super.prototype.profilazione.call(this);
        out += console.log(" e corre " +this.velocita +" metri al secondo");
        return out;
    };


    Atleta.prototype.corri = function (secondi) {
        var distanza_percorsa=0;
        distanza_percorsa=this.velocita*secondi;
        distanza_percorsa -= this.rallenta(distanza_percorsa);
        return Math.round(distanza_percorsa);
    }
    Atleta.prototype.rallenta = function(distanza) {
        return ((distanza / 10) * (0,002 * this.eta))/1000;
    }
    return Atleta;

}(Persona));


var mario = new Persona("mario",50);
var maria = new Persona("maria",54);
var giacomo = new Atleta("giacomo",23,6);
var giorgio = new Atleta("giorgio",25,7);
var jessica = new Atleta("jessica",18,8);

mario.profilazione();
maria.profilazione();
giacomo.profilazione();
giorgio.profilazione();
jessica.profilazione();

console.log("giacomo in 60 secondi corre "+giacomo.corri(60));
console.log("giorgio in 60 secondi corre "+giorgio.corri(60));
console.log("jessica in 60 secondi corre "+jessica.corri(60));